import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
import BootstrapVue3 from "bootstrap-vue-3";

import "./app.scss";
const app = createApp(App);
app.use(BootstrapVue3);
app.mount("#app");
